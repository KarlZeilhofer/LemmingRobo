/* Copyright (C) 2013-2015 Kristian Lauszus, TKJ Electronics. All rights reserved.

 This software may be distributed and modified under the terms of the GNU
 General Public License version 2 (GPL2) as published by the Free Software
 Foundation and appearing in the file GPL2.TXT included in the packaging of
 this file. Please note that GPL2 Section 2[b] requires that all works based
 on this software must also be made publicly available under the terms of
 the GPL2 ("Copyleft").

 Contact information
 -------------------

 Kristian Lauszus, TKJ Electronics
 Web      :  http://www.tkjelectronics.com
 e-mail   :  kristianl@tkjelectronics.com
*/

 // adopted by Karl Zeilhofer (www.zeilhofer.co.at)

#include "EEPROMAnything.h"
#include "config.h"


// public objects
  CFG_Parameters P; //  Struct for all the parameters

// private constants:
  static const uint8_t eepromVersion = 27; // EEPROM version - used to restore the EEPROM values if the configuration struct have changed
  static const uint8_t initFlagsAddr = 0; // Set the first byte to the EEPROM version
  static const uint8_t configAddr = 1; // Save the Parameters starting from this location

// private variables:
  static int iteratorIndex=0; // IMPORTANT: backup and restore this value in private functions. 
  static CFG_Parameter* activeParameter=NULL; // to this parameter the global commands are applied
  // table printing:
  static int8_t columnsIndices[10]; // each entry holds an index. "-1" for no parameter-index
    // columns range: 0 = default column (dt)
    // visible columns: 1...9
  static bool printTable = false;

// private functions:
static void setAndWriteDefaults();
static int indexByParameter(CFG_Parameter* p);
static void initParameter(CFG_Parameter* p, 
                          const char* name, float value, const char* unit, float delta, bool absolute, 
                          char activationChar);
static void iteratorReset();
static CFG_Parameter* nextParameter();
static CFG_Parameter* parameterByIndex(uint8_t i);
static void printParameter(CFG_Parameter* p);
static void printValue(float v);
static void readEEPROMValues();
static void restoreColumnsIndices();










// PUBLIC FUNCTIONS //
// CFG_xxx()


// load defaults from flash or restore values from EEPROM
void CFG_init() {
  uint8_t initFlag;
  EEPROM_readAnything(initFlagsAddr, initFlag);
  if (initFlag != eepromVersion) { // Check if the EEPROM version matches the current one
    setAndWriteDefaults();
    EEPROM_updateAnything(initFlagsAddr, eepromVersion); // After the default values have been restored, set the flag
    return; // Indicate that the values have been restored
  }
  readEEPROMValues();


  restoreColumnsIndices();
}


// global 'L'
void CFG_printList()
{
  CFG_Parameter* p;

  iteratorReset();
  while(p = nextParameter()) // iterator loop
  {
    Serial.print("[");
    Serial.print(p->activationChar);
    Serial.print("] ");
    printParameter(p);
    Serial.println();
    delay(10);
  }
}


void CFG_printTableHeader()
{
  CFG_Parameter* p=NULL;
  Serial.print('%'); // matlab-comment-token
  Serial.print("values: dt ");
  for(uint8_t c=1; c<10; c++)
  {
    if(columnsIndices[c] >= 0) // check if column is active
    {
      if(p=parameterByIndex(columnsIndices[c])) // check, if we get a non-null pointer
      {
        Serial.print(p->name); 
        Serial.print(" ");
      }
    }else
    {
      Serial.print("Col");
      Serial.print(c);
      Serial.print(" ");
    }
  }
  Serial.println();

  // Units:
  Serial.print('%'); // matlab-comment-token
  Serial.print("units: s ");
  for(uint8_t c=1; c<10; c++)
  {
    if(columnsIndices[c] >= 0) // check if column is active
    {
      if(p=parameterByIndex(columnsIndices[c])) // check, if we get a non-null pointer
      {
        Serial.print(p->unit); 
        Serial.print(" ");
      }else
      {
        Serial.print("Col");
        Serial.print(c);
        Serial.print(" ");
      }
    }
  }
  Serial.println();

}

// call this function periodically, 
// it is enabled/disabled with 'P'
void CFG_printTableRow(float dt)
{
  if(printTable)
  {
    CFG_Parameter* p=NULL;
    printValue(dt); // first column is allways dt
    Serial.print(" ");
    for(uint8_t c=1; c<10; c++)
    {
      if(columnsIndices[c] >= 0) // check if column is active
      {
        if(p=parameterByIndex(columnsIndices[c]))
        {
          printValue(p->v);
          Serial.print(" ");
        }
      }
      else
      {
        Serial.print("0.00 ");
      }
    }
    Serial.println();
  }
}

void CFG_updateEEPROM() {
  EEPROM_updateAnything(configAddr, P);
}


void CFG_userInput(char c)
{
  CFG_Parameter* p;


  switch(c)
  {
    case 'L': CFG_printList(); break;
    case 'P': 
      printTable = !printTable;
      CFG_printTableHeader();
      break;
    case 'S': 
      CFG_updateEEPROM(); 
      Serial.println("Gespeichert");
    break;
    case 'V': printParameter(activeParameter); break;

    case '+':
      if(activeParameter->changeAbsolut_notRelative)
        activeParameter->v += activeParameter->delta;
      else
        activeParameter->v *= (1.0f+activeParameter->delta);
      printParameter(activeParameter); Serial.println();
      break;
      
    case '-':
      if(activeParameter->changeAbsolut_notRelative)
        activeParameter->v -= activeParameter->delta;
      else
        activeParameter->v /= (1.0f+activeParameter->delta);
      printParameter(activeParameter); Serial.println();
      break; 
    default:
      if(c >= '0' && c<= '9') // column printing assignment
      {
        int col = c-'0';
        // First select parameter, then press 1-9, if it should be printed, 
        // or 0, if it should not be printed. 
        if(col > 0) // enable printing
        {
          if(columnsIndices[col] != -1)
          {
            // disable printing for current parameter in this column:
            parameterByIndex(columnsIndices[col])->column = 0;
          }

          // check if this parameter is used in another column, and remove it from there
          for(int i=1; i<=9; i++) 
          {
            if(indexByParameter(activeParameter) == columnsIndices[i])
            {
              columnsIndices[i]=-1;
            }
          }
          activeParameter->column = col;
          columnsIndices[col] = indexByParameter(activeParameter);
        }else
        {
          // disable printing vor activeParameter
          activeParameter->column = 0;
          columnsIndices[col] = -1;
        }

        CFG_printTableHeader();
      } // endif
      else // activate a parameter
      {
        iteratorReset();
        while(p = nextParameter()) // iterator loop
        {
          if(p->activationChar == c)
          {
            activeParameter = p;
            printParameter(p); Serial.println();
          }
        }
      } // endelse
  } // endswitch

}











// PRIVATE FUNCTIONS //




static void setAndWriteDefaults() 
{
  initParameter(&P.dtMin, PSTR("dtMin"), 10e-3, PSTR("s"), 0.1, false, '<');

  // Winkelregler
  initParameter(&P.phiNull, PSTR("phiNull"), 180.0f, PSTR("deg"), 0.2, true, 'r');
  initParameter(&P.phiSteuer, PSTR("_phiSteuer"), 0.0f, PSTR("deg"), 0.1, true, 't');
  initParameter(&P.phiLimit, PSTR("phiLimit"), 8.00f, PSTR("deg"), 0.2, true, 'z');
  initParameter(&P.phiFehler, PSTR("_phiFehler"), 0.0f, PSTR("deg"), 0.0f, false, 'i'); // in °
  initParameter(&P.phiReg_P, PSTR("phiReg_P"), 285.96e-3, PSTR("pwm/deg"), 0.1, false, 'q');
  initParameter(&P.phiReg_I, PSTR("phiReg_I"), 3.87e-6, PSTR("pwm/deg.s"), 0.1, false, 'w');
  initParameter(&P.phiReg_D, PSTR("phiReg_D"), 937.51e-3, PSTR("pwm/ deg/s"), 0.1, false, 'e');

  // Positionsregler
  initParameter(&P.pwmMotor, PSTR("_pwmMotor"), 0.0f, PSTR("pwm"), 0.1, true, 'h');
  initParameter(&P.pwmFilter_tau, PSTR("pwmFilter_tau"), 3.14f, PSTR("s"), 0.1, false, 'j');
  initParameter(&P.kMotor, PSTR("kMotor"), 0.64, PSTR("m/s /pwm"), 0.02, false, 'k');
  initParameter(&P.vMotor, PSTR("_vMotor"), 0.0f, PSTR("m/s"), 0.0f, false, 'l'); 

  initParameter(&P.pos, PSTR("_pos"), 0.0f, PSTR("m"), 0.0f, false, 'y');
  initParameter(&P.posSoll, PSTR("_posSoll"), 0.0f, PSTR("m"), 0.0f, false, 'x');
  initParameter(&P.posFehler, PSTR("_posFehler"), 0.0f, PSTR("m"), 0.0f, false, 'c');
  initParameter(&P.posReg_P, PSTR("posReg_P"), 1315.52e-3, PSTR("deg/m"), 0.1, false, 'a');
  initParameter(&P.posReg_I, PSTR("posReg_I"), 500.00e-3, PSTR("deg/m.s"), 0.1, false, 's');
  initParameter(&P.posReg_D, PSTR("posReg_D"), 6.2f, PSTR("deg/ m/s"), 0.1, false, 'd');
  initParameter(&P.posReg_DD, PSTR("posReg_DD"), 1464.10e-3, PSTR("deg/ (m/s)^2"), 0.1, false, 'D');

  initParameter(&P.motorEin, PSTR("motorEin"), 1.0f, PSTR("binaer"), 1, true, 'v'); 

  // Selektiv-Filter:
  initParameter(&P.phiBeschl, PSTR("_phiBeschl"), 0.0f, PSTR("deg"), 0, true, 'b');
  initParameter(&P.omegaGyro, PSTR("_omegaGyro"), 0.0f, PSTR("deg/s"), 0, true, 'n');
  initParameter(&P.phiGyro, PSTR("_phiGyro"), 0.0f, PSTR("deg"), 0, true, 'm');
  initParameter(&P.phiSelekt, PSTR("_phiSelekt"), 0.0f, PSTR("deg"), 0, true, '#');

  initParameter(&P.omegaLenk_P, PSTR("omegaLenk_P"), 1933.55e-6f, PSTR("PWM/(deg/s)"), 0.05, false, '(');
  initParameter(&P.omegaLenk_I, PSTR("omegaLenk_I"), 56.21e-3f, PSTR("PWM/deg"), 0.05, false, ')');

  CFG_updateEEPROM(); // TODO: optimize storage usage, since we have volatile and non-volatile parameters. 
}


// This function returns the "array-index" of the parameter p
// the index is derived from the memory position in the structure like in an array. 
// returns -1, if parameter not found in the printing-table
// see also parameterByIndex()
static int indexByParameter(CFG_Parameter* p)
{
  CFG_Parameter* pTest;
  int ret=-1;
  int i=0;

  int itBackup = iteratorIndex;
  iteratorReset();
  while(pTest = nextParameter())
  {
    if(pTest == p)
      return i;
    i++;
  }
  iteratorIndex = itBackup;

  return ret;
}

static void initParameter(CFG_Parameter* p, 
  const char* name, float value, const char* unit, float delta, bool absolute, 
  char activationChar)
{
  strncpy_P(p->name, name, CFG_MAX_NAME_LEN); p->name[CFG_MAX_NAME_LEN]=0;
  p->v = value;
  strncpy_P(p->unit, unit, CFG_MAX_UNIT_LEN); p->unit[CFG_MAX_NAME_LEN]=0;
  p->delta = delta;
  p->changeAbsolut_notRelative = absolute;
  p->activationChar = activationChar;
  p->column = 0; // disable by default
}

// see also nextParameter()
static void iteratorReset()
{
  iteratorIndex = 0;
}

// iterator function
// if it returns a NULL pointer, no more parameters available
// use iteratorReset()
static CFG_Parameter* nextParameter()
{
  CFG_Parameter* ret = NULL;

  if(ret = parameterByIndex(iteratorIndex))
  {
    iteratorIndex++;
  }

  return ret;
}


// all parameters in the struct are indexed like in an array
// this function returns a pointer to the parameter with the index i. 
static CFG_Parameter* parameterByIndex(uint8_t i)
{
  CFG_Parameter* ret = NULL;

  if(i < sizeof(CFG_Parameters)/sizeof(CFG_Parameter))
  {
    ret = (CFG_Parameter*)(&P) + i;
  }
  return ret;
}

static void printParameter(CFG_Parameter* p)
{
  Serial.print(p->name);
  Serial.print("=");
  printValue(p->v);
  Serial.print(" ");
  Serial.print(p->unit);
}

// print value without line break!
static void printValue(float v)
{
  if(fabs(v) < 3e-6)
  {
    Serial.print(v*1e9);
    Serial.print("e-9");
  }
  else if(fabs(v) < 3e-3)
  {
    Serial.print(v*1e6);
    Serial.print("e-6");
  }
  else if(fabs(v) < 3)
  {
    Serial.print(v*1e3);
    Serial.print("e-3");
  }
  else
  {
    Serial.print(v);
  }
}

static void readEEPROMValues() {
  EEPROM_readAnything(configAddr, P);
}

static void restoreColumnsIndices()
{
  CFG_Parameter* p;
  int itBackup = iteratorIndex;
  iteratorReset();

  for(int c=1; c<10; c++)
  {
    columnsIndices[c]=-1; // set to no parameter
  }

  while(p = nextParameter())
  {
    columnsIndices[p->column] = indexByParameter(p);
  }
  iteratorIndex = itBackup;
}


// imu.h

// IMU steht für Inertial Measurement Unit, was übersetzt soviel heißt wie 
// Inertialsystem-Messeinheit
// Solch eine Einheit liefert Messwerte über eigenen Ort, eigene Geschwindigkeit und Lage
// Hier wird eine vereinfachte Messeinheit verwendet, die dazu dient, den aktuellen
// Winkel des Lemming-Robos zu messen. 

// 3D Beschleunigungssensor und 3D Drehgeschwindigkeitssensor
// Chip: MPU-6050
// Code von Karl Zeilhofer und "Arduino User JohnChi"
// Juli 2015 und August 17, 2014


#ifndef __IMU_H
#define __IMU_H

#ifdef __cplusplus
extern "C" {
#endif


typedef struct _IMU_Objekt
{
	int16_t aX;
	int16_t aY;
	int16_t aZ;
	int16_t Tmp;
	int16_t omegaX;
	int16_t omegaY;
	int16_t omegaZ;
}IMU_Objekt;

extern IMU_Objekt imu; // IMU-Objekt in imu.ino, erlaubt globalen Zugriff auf die Messdaten
extern float omegaNullVersatz;

void IMU_einrichten();
void IMU_werteEinlesen();


#ifdef __cplusplus
}
#endif


#endif
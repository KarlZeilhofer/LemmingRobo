//tools.h

#ifndef __TOOLS_H
#define __TOOLS_H

#ifdef __cplusplus
extern "C" {
#endif


void checkSerialData();
void printMenu();
void calibrateAcc();
void calibrateMotor();
void testMotorSpeed(float *leftSpeed, float *rightSpeed, float leftScaler, float rightScaler);
void printValues();
void setValues(char *input);
bool checkMinMax(int16_t *array, uint8_t length, int16_t maxDifference);



#ifdef __cplusplus
}
#endif


#endif
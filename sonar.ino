// sonar.ino

#include "sonar.h"

void Sonar_init(Sonar_Object* obj, uint8_t triggerPin, uint8_t echoPin)
{
	obj->triggerPin = triggerPin;
	obj->echoPin = echoPin;
	obj->duration_us = 0;
	obj->distance_mm = 0;
	pinMode(obj->triggerPin, OUTPUT);
	pinMode(obj->echoPin, INPUT);
}


void Sonar_measure(Sonar_Object* obj, int16_t maxLen_mm)
{
	// The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
	// Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
	digitalWrite(obj->triggerPin, HIGH);
	delayMicroseconds(2);
	digitalWrite(obj->triggerPin, LOW);

  	obj->duration_us = pulseIn(obj->echoPin, HIGH, ((int32_t)maxLen_mm*1024*2)/348); 
  	obj->distance_mm = ((int32_t)obj->duration_us*348)/1024/2; // scaled to 1024 for faster calculation!
}

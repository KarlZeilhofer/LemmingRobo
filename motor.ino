// motor.ino

#include "motor.h"
#include "pins.h"

// gemessene spannung bez. Haftreibung (Stillstand --> Bewegung):
// Linker Motor: 
// 		vorwärts: 84
//		rückwärts: 62
// Rechter Motor: 
// 		vorwärts: 72
//		rückwärts: 99

// gemessene spannung bez. Coulombreibung (Bewegung --> Stillstand):
// Linker Motor: 
// 		vorwärts: 45
//		rückwärts: 45
// Rechter Motor: 
// 		vorwärts: 34
//		rückwärts: 42


float richtungsVersatz = 0; // die Motoren bevorzugen eine Drehrichtung. 
	// ohne dieser Kompensation tendiert der Robo sich immer in eine bestimmte 
	// Richtung zu drehen (nach links beim Prototypen). 
	// ein positiver Wert entspricht entsprechend einer "korrigierenden Kraft" nach rechts. 
static int reibGrenze = 0;



void Motor_L(int16_t geschwindigkeit) // +- 255, plus = nach vor, minus = zurück
{
	int16_t spannung = -geschwindigkeit; // Drerichtungsumkehr für linken Motor!

	if(spannung < 0) // vorwärts
		spannung = min(-reibGrenze, spannung);
	if(spannung > 0)
		spannung = max(reibGrenze, spannung);
	SpannungEinstellen(Motor_L_Plus_Pin, Motor_L_Minus_Pin, spannung); 
}

void Motor_R(int16_t geschwindigkeit) // +- 255, plus = nach vor, minus = zurück
{
	int16_t spannung = geschwindigkeit;

	if(spannung < 0) // rückwärts
		spannung = min(-reibGrenze, spannung);
	if(spannung > 0)
		spannung = max(reibGrenze, spannung);

	SpannungEinstellen(Motor_R_Plus_Pin, Motor_R_Minus_Pin, spannung); 
}

void Motor_beep(int32_t f, int32_t T_ms)
{
	int32_t N = (f*T_ms)/1000L;
	int32_t Th_us = 1e6/(2*f);

	for(int32_t i = 0; i<N; i++)
	{
		digitalWrite(Motor_L_Plus_Pin, 0);
		digitalWrite(Motor_L_Minus_Pin, 1);
		digitalWrite(Motor_R_Plus_Pin, 0);
		digitalWrite(Motor_R_Minus_Pin, 1);
		delayMicroseconds(Th_us);
		digitalWrite(Motor_L_Plus_Pin, 1);
		digitalWrite(Motor_L_Minus_Pin, 0);
		digitalWrite(Motor_R_Plus_Pin, 1);
		digitalWrite(Motor_R_Minus_Pin, 0);
		delayMicroseconds(Th_us);
	}
	digitalWrite(Motor_L_Plus_Pin, 0);
	digitalWrite(Motor_L_Minus_Pin, 0);
	digitalWrite(Motor_R_Plus_Pin, 0);
	digitalWrite(Motor_R_Minus_Pin, 0);
}


// Die PWM-Frequenz hat gemessene 975Hz. 
// Diese wird direkt von der Arduino-Bibliothek eingestllt. 
void SpannungEinstellen(int8_t plusPin, int8_t minusPin, int16_t spannung)
{
	bool rechtsdrehend = spannung >= 0;
	if(rechtsdrehend)
	{
		spannung = min(spannung, 255); // Beschränkung auf 0..255
		
		// "verkehrte Logik" wegen Active-Low-Eingänge der Doppel-H-Brücke
		digitalWrite(plusPin, 0); // Plus-Anschluss des Motors auf 5V klemmen. 
		analogWrite(minusPin, spannung); // PWM einstellen (--> quasi analoge Spannung)
	}else{ // rückwärts
		spannung = spannung*(-1); 
		spannung = min(spannung, 255); // Beschränkung auf 0..255

		// "verkehrte Logik" wegen Active-Low-Eingänge der Doppel-H-Brücke
		digitalWrite(minusPin, 0); // Minus-Anschluss des Motors auf 5V klemmen. 
		analogWrite(plusPin, spannung); // PWM einstellen (--> quasi analoge Spannung)
	}
}



// lemming-robo-firmware

#include "config.h"
#include "defs.h"
#include "i2c.h"
#include "imu.h"
#include "motor.h"
#include "pins.h"
#include "sonar.h"
#include "tools.h"

#include <stdio.h>


// Sonar objects:
Sonar_Object _sonarL; Sonar_Object* sonarL = &_sonarL;
Sonar_Object _sonarR; Sonar_Object* sonarR = &_sonarR;

float omegaVersatz_querachse; // in LSB
float omegaVersatz_hochachse; // in LSB, Hochachse dient der ausregelung des Geradeausfahrens. 



/* Used for timing */
static uint32_t generalTimer_us; // timer_us used for ...
static uint32_t pidTimer_us; // timer_us used for the PID loop
static uint32_t imuTimer_ms; // This is used to set a delay between sending IMU values

static bool roboLiegt = true; // Flag, ob der Robo gerade liegt oder ob er balanciert
static bool roboSoebenAufgerichtet = false; // Flag, ob der Robo gerade liegt oder ob er balanciert

float winkelDesBeschleunigungsvektors();
float drehgeschwindigkeitUmQuerachse();
float drehgeschwindigkeitUmHochachse();
bool drehgeschwindigkeitUmQuerachseKalibrieren();
bool drehgeschwindigkeitUmHochachseKalibrieren();
bool checkMinMax(int16_t *array, uint8_t length, int16_t maxDifference);
void parameterEinstellen();
float lenken();

float winkelRegler(float dt);
float positionsRegler(float dt);
float hochachsRegler(float dt);

// Einmalig alles einrichten
void setup() // dieser Funktionsname darf nicht verändert werden!
{
	Serial.begin(115200); // Serielle Schnittstelle über USB zum Computer einrichten
	Serial.println("'L' listet alle Parameter auf.");
	Serial.println("'S' speichert die neuen Werte.");

	CFG_init();

	IMU_einrichten();
	Pins_einrichten();
	Sonar_init(sonarL, Sonar_L_Trig_Pin, Sonar_L_Echo_Pin);
	Sonar_init(sonarR, Sonar_R_Trig_Pin, Sonar_R_Echo_Pin);

	IMU_werteEinlesen();
	P.phiBeschl.v = winkelDesBeschleunigungsvektors();
	P.phiGyro.v = P.phiBeschl.v;

	/* Calibrate gyro zero value */

	Motor_beep(1000, 100);
	delay(500);
	Motor_beep(1000, 100);
	delay(500);
	Motor_beep(1000, 100);
	delay(500);
	Motor_beep(1000, 500);



	while (drehgeschwindigkeitUmQuerachseKalibrieren()); // Run again if the robot is moved while calibrating
	while(drehgeschwindigkeitUmHochachseKalibrieren());

	Motor_beep(1000, 100);
	delay(100);
	Motor_beep(2000, 300);



	#ifdef ENABLE_TOOLS
		printMenu();
	#endif


	/* Setup timing */
	generalTimer_us = micros();
	pidTimer_us = generalTimer_us;
	imuTimer_ms = millis();


	P.phiBeschl.v = winkelDesBeschleunigungsvektors();
	P.phiGyro.v = P.phiBeschl.v;
	P.phiSelekt.v = P.phiBeschl.v;

	// 2s simulieren, um den Filter einzuschwingen:
	// (verhindert anfängliches drehen der Räder nach dem Strom einschalten)
	for(int i=0; i<200; i++){
		selektiverFilter(1e-2);	
	}
	
}

// Endlosschleife
void loop() // dieser Funktionsname darf nicht verändert werden!
{
	/* Calculate winkel */
	IMU_werteEinlesen();

	int16_t omega = drehgeschwindigkeitUmQuerachse(); // in LSB

	// atan2 outputs the value of -π to π (radians) - see http://en.wikipedia.org/wiki/Atan2
	// We then convert it to 0 to 2π and then from radians to degrees
	P.phiBeschl.v = winkelDesBeschleunigungsvektors();

	uint32_t timer_us = micros();
	// This fixes the 0-360 transition problem when the accelerometer angle jumps between 0 and 360 degrees
	if ((P.phiBeschl.v < 90 && P.phiSelekt.v > 270) || (P.phiBeschl.v > 270 && P.phiSelekt.v < 90)) { // TODO: check values of phiSelect
		P.phiGyro.v = P.phiBeschl.v;
		P.phiSelekt.v = P.phiBeschl.v;
	} else {
		P.omegaGyro.v = ((float)omega - omegaVersatz_querachse) / 131.0f; // Convert to deg/s
		float dt = (float)(timer_us - generalTimer_us) / 1000000.0f;
		P.phiGyro.v += P.omegaGyro.v * dt; // Gyro angle is only used for debugging
		selektiverFilter(dt); // resultat in P.phiSelekt
	}
	generalTimer_us = timer_us;
	//Serial.print(P.phiBeschl.v);Serial.print('\t');Serial.print(P.phiGyro.v);Serial.print('\t');Serial.println(winkel);

	/* Drive motors */

	// Wenn der Robo liegt, muss er aufgerichtet werden auf +-10° zur Vertikalen, befor er zum balancieren anfängt. 
	// Wenn er schon balanciert, muss der Winkel um +-45° abweichen, damit er aufhört. 
	float dt;
	if ((roboLiegt && (P.phiSelekt.v < P.phiNull.v - 1 || P.phiSelekt.v > P.phiNull.v + 1)) || 
		(!roboLiegt && (P.phiSelekt.v < P.phiNull.v - 45 || P.phiSelekt.v > P.phiNull.v + 45))) {
		roboLiegt = true; // The robot is in a unsolvable position, so turn off both motors and wait until it's vertical again
		Motor_L(0);
		Motor_R(0);
		dt=0;
		lenken(); // kümmert sich um die LEDs
	} else {
		timer_us = micros();
		float dt = (float)(timer_us-pidTimer_us)*1e-6;

		// Verzögerung, sodass minimale Abtastzeit eingehalten wird
		// (zu schnelles Reglen führt zu numerischen instabilitäten)
		while(dt < P.dtMin.v)
		{
			timer_us = micros();
			dt = (float)(timer_us-pidTimer_us)*1e-6;
		}

		if(roboLiegt)
		{
			roboLiegt = false; // wurde gerade aufgestellt. 
			roboSoebenAufgerichtet = true;
		}

		P.pwmMotor.v = winkelRegler(dt); // unterlagerter Regler
		P.phiSteuer.v = positionsRegler(dt); // überlagerter Regler
		float pwmLinksRechtsAusgleich = hochachsRegler(dt); // in PWM (+- 1.0)

		pidTimer_us = timer_us;


		float pwmL = (P.pwmMotor.v - pwmLinksRechtsAusgleich)*255.0f ;
		float pwmR = (P.pwmMotor.v + pwmLinksRechtsAusgleich)*255.0f ;

		Motor_L(pwmL * P.motorEin.v);
		Motor_R(pwmR * P.motorEin.v);

	}
	CFG_printTableRow(dt);
	parameterEinstellen();

	roboSoebenAufgerichtet = false;
}



// Der Positionsregler ist der Kern der Regelung. 
// Er ist dem Winkel-Regler überlagert, und hat die Struktur: PDD²
// Seine Sollgröße ergibt sich aus der Funktion posSoll() und 
// seine Messgröße ist das Integral der gefilterten Motorspannung, was in etwa 
// einer fikiven Position entspricht, da der Robo keine Wegsensoren verbaut hat. 
// Sein Ausgang ist der Winkel phiSteuer, also der Sollwinkel des Winkel-Reglers. 
// 
// dt: Zeitschritt in s seit letztem Aufruf
// gibt phiSteuer in ° zurück
float positionsRegler(float dt)
{
	static float pwmGefiltert=0;
	//float vSoll = 0.1; // m/s

	if(roboSoebenAufgerichtet)
	{
		P.pos.v = 0; // Integrator zurücksetzten, wenn Robo gelegen ist. 
		pwmGefiltert = 0; // Filter zurücksetzten, wenn Robo gelegen ist. 
		P.posSoll.v = posSoll(0, true);
		return 0;
	}
	else // nach einer pause nicht integrieren!
	{
		pwmGefiltert = pwmGefiltert + (P.pwmMotor.v - pwmGefiltert)*P.pwmFilter_tau.v*dt; // IIR-Filter 1.Ordnung
		P.vMotor.v = pwmGefiltert * P.kMotor.v; // Umrechnung auf m/s
		P.pos.v += P.vMotor.v*dt; // integrierte Geschwindigkeit ergibt Position
	}

	P.posSoll.v = posSoll(dt, false);

	P.posFehler.v = P.posSoll.v - P.pos.v;

	float winkel=0;
	static float posFehlerAlt = 0; // wird für das Differenzieren benötigt. 

	winkel += P.posReg_P.v * P.posFehler.v; // P-Anteil
	winkel = constrain(winkel, -P.phiLimit.v/4, +P.phiLimit.v/4); 
		// P-Anteil (Feder) darf bei großen Abweichungen nicht dominieren!

	float dX_dt = (P.posFehler.v - posFehlerAlt)/dt;
	static float dX_dt_alt = 0;
	float dX2_dt2 = (dX_dt - dX_dt_alt)/dt;

	winkel += P.posReg_D.v * dX_dt; // D-Anteil
	winkel += P.posReg_DD.v * dX_dt; // D²-Anteil

	posFehlerAlt = P.posFehler.v;
	dX_dt_alt = dX_dt;

	P.phiSteuer.v = constrain(winkel, -P.phiLimit.v, +P.phiLimit.v);
	// if(P.phiSteuer.v != winkel)
	// 	P.pos.v -= P.vMotor.v*dt; // Integration rückgängig machen, wenn limit erreicht ist. 

	return P.phiSteuer.v;
}

// Der Winkelregler versucht mit dem Eingang phiSelekt als Messwert und phiSteuer als Sollgröße
// diesen Sollwert zu erreichen. 
// Derzeit ist der Regler ein einfacher PD-Regler. 
// dt: Zeitschritt in s seit letztem Aufruf
// gibt PWM-Wert zurück (-255...+255)
float winkelRegler(float dt)
{
	static float winkelFehlerAlt=0;  // Für Differential-Regler
	static float winkelFehlerIntegral=0; // Für Integral-Regler


	// Integrator zurücksetzten, wenn Robo gelegen ist
	if(roboSoebenAufgerichtet)
	{
		dt = P.dtMin.v;
		winkelFehlerIntegral = 0; 
	}
		

	P.phiFehler.v = -(P.phiNull.v + P.phiSteuer.v) + P.phiSelekt.v; // in °
	winkelFehlerIntegral += P.phiFehler.v;


	float pwm = 0;
	pwm += P.phiReg_P.v * P.phiFehler.v; // P-Anteil
	//pwm += P.phiReg_I.v * winkelFehlerIntegral; // I-Anteil // TODO: I-Anteil mit limit einbauen. 
	pwm += P.phiReg_D.v * (P.phiFehler.v - winkelFehlerAlt); // D-Anteil

	P.pwmMotor.v = constrain(pwm, -1.0, +1.0);
	// TODO: antiwindup

	winkelFehlerAlt = P.phiFehler.v;

	return P.pwmMotor.v;
}


// gitb pwmLinksRechtsAusgleich zurück, der die Motor-PWM dann asymmetrisch beeinflusst
float hochachsRegler(float dt)
{
	static float omegaFehlerIntegral = 0;
	static float omegaGefiltert=0;
	static float omegaSollGefiltert=0;
	if(roboSoebenAufgerichtet)
	{
		omegaFehlerIntegral = 0; 
		omegaGefiltert = 0;
		omegaSollGefiltert=0;
		dt = P.dtMin.v;
	}

	float omega = (drehgeschwindigkeitUmHochachse()-omegaVersatz_hochachse)/131.0; // in °/s, positiv = linkskurve, negativ = rechtskurve

	float tau = 0.1;
	omegaGefiltert = omegaGefiltert + (omega - omegaGefiltert)*tau*dt; // IIR-Filter 1.Ordnung
	omegaGefiltert = omega; // filter umgehen

	float omegaSoll = lenken(); // in °/s
	omegaSollGefiltert = omegaSollGefiltert + (omegaSoll - omegaSollGefiltert)*tau*dt; // IIR-Filter 1.Ordnung
	omegaSollGefiltert = omegaSoll; // filter umgehen

	float omegaFehler = omegaSollGefiltert - omegaGefiltert; 
	omegaFehlerIntegral += omegaFehler*dt; // in °

	// Adaptiver Winkel, falls er erzwungen wird:
	omegaFehlerIntegral -= 10*dt*omegaFehlerIntegral; 

	float kI = P.omegaLenk_I.v;
	float kP = P.omegaLenk_P.v;

	return omegaFehler*kP + omegaFehlerIntegral*kI;
}

















// Definition der Koordinaten (abhängig von der Montage des MPU6050 auf dem Robo):
// Hochachse: y, positiv nach oben
// Querachse: x, positiv nach rechts
// Längsachse: z, positiv nach hinten

// Obige Achsen werden übersetzt auf folgende Achsen (fix)
// Laengs (positiv nach vorne)
// Hoch (positiv nach oben)

// Winkel: 
// bei aufrechter Position: 180°
// auf der Nase liegend: 270°
// auf dem Rücken liegend: 90°

float winkelDesBeschleunigungsvektors()
{
	IMU_werteEinlesen();
	int16_t aHoch = -imu.aZ; 
	int16_t aLaengs = -imu.aX;
	// atan2 outputs the value of -π to π (radians) - see http://en.wikipedia.org/wiki/Atan2
	// We then convert it to 0 to 2π and then from radians to degrees
	return  
		(
			atan2
			(
				(float)aLaengs /*- P.aLaengsachseNullVersatz.v*/, 
				-((float)aHoch /*- P.aHochachseNullVersatz.v)*/) // TOOD
			) 
			+ PI
		) * RAD_TO_DEG;

}

// TODO: eindeutschen
bool drehgeschwindigkeitUmQuerachseKalibrieren() {
	int16_t gyroBuffer[25];
	for (uint8_t i = 0; i < 25; i++) {
		IMU_werteEinlesen();
		gyroBuffer[i] = drehgeschwindigkeitUmQuerachse();
		delay(10);
	}
	if (!checkMinMax(gyroBuffer, 25, 2000)) {
	  Serial.println(F("Gyro calibration error"));
	  return 1;
	} 
	for (uint8_t i = 0; i < 25; i++)
		omegaVersatz_querachse += gyroBuffer[i];
		omegaVersatz_querachse /= 25.0f;
	return 0;
}

bool drehgeschwindigkeitUmHochachseKalibrieren() {
	int16_t gyroBuffer[25];
	for (uint8_t i = 0; i < 25; i++) {
		IMU_werteEinlesen();
		gyroBuffer[i] = drehgeschwindigkeitUmHochachse();
		delay(10);
	}
	if (!checkMinMax(gyroBuffer, 25, 2000)) {
	  Serial.println(F("Gyro calibration error"));
	  return 1;
	} 
	for (uint8_t i = 0; i < 25; i++)
		omegaVersatz_hochachse += gyroBuffer[i];
		omegaVersatz_hochachse /= 25.0f;
	return 0;
}



bool checkMinMax(int16_t *array, uint8_t length, int16_t maxDifference) { // Used to check that the robot is laying still while calibrating
  int16_t min = array[0], max = array[0];
  for (uint8_t i = 1; i < length; i++) {
    if (array[i] < min)
      min = array[i];
    else if (array[i] > max)
      max = array[i];
  }
  return max - min < maxDifference;
}


// Macht nur die Koordinatenzuweisung
// IMU_werteEinlesen muss vorher aufgerufen werden!
// Kippt der Robo nach vorne, liefert die Funktion einen positiven Wert!
float drehgeschwindigkeitUmQuerachse()
{
	return imu.omegaY;
}

// rechtsdrehend entsprechend einem nach oben schauenden Vektor
float drehgeschwindigkeitUmHochachse() 
{
	return -imu.omegaZ;
}



void parameterEinstellen()
{
	if(Serial.available())
	{
		CFG_userInput(Serial.read());
	}
}

// verwendet phiBeschl und phiGyro
// und berechnet phiSelekt
// Dabei wird der Winkel aus dem Beschleunigungssensor stark tiefpassgefiltert, 
// um Schwankungen, hervorgerufen durch Bewegungsbeschleunigung, herauszumitteln. 
// Der integrierte Winkel vom Gyro wird Hochpassgefiltert, um das Driften zu eliminieren. 
void selektiverFilter(float dt)
{
	float phiB = P.phiBeschl.v;
	float phiG = P.phiGyro.v;

	static float phiHP=0; // Hochpass 
	static float phiTP=0; // Tiefpass
	static float phiG_alt;
	float deltaPhiGyro = phiG - phiG_alt;

	static bool ersterBesuch=true;
	static const float tau = 1; // in s, Filterzeitkonstante

	if(ersterBesuch)
	{
		ersterBesuch = false;
		phiHP = phiB;
		phiTP = phiB;
		deltaPhiGyro = 0;
	}

	phiTP = phiTP + (phiB-phiTP)*dt/tau;
	phiHP = phiHP + deltaPhiGyro - phiHP*dt/tau;

	phiG_alt = phiG;

	P.phiSelekt.v = phiTP + phiHP; 
}

float posSoll(float dt, bool reset)
{
	//return 0;

	const float vMax = P.kMotor.v/7; // maximale sollgeschwindigkeit = 50% der maximal möglichen Geschwindigkeit
		// die Motoren benötigen noch eine Reserve, um ausbalancieren zu können. 

	static float x=0; // soll weg in m (zeitabhängig)

	if(reset)
		x=0;

	static bool vorwaerts=true;
	// if(vorwaerts && x < 1)
	// {
	// 	x += vMax*dt;
	// }
	// if(!vorwaerts && x > 0)
	// {
	// 	x -= vMax*dt;
	// }

	x += vMax*dt;
	return x;


	// // zyklisches Umschalten zwischen vorwärts und rückwärts
	// if(vorwaerts && x >= 1)
	// 	vorwaerts = false;
	// if(!vorwaerts && x <= 0)
	// 	vorwaerts = true;


	//x = x + (vMax*sin(2*M_PI*0.05*dt))*dt;

	// Tiefpassfilter 2. Ordnung, sodass die Trajektorie schön glatt ist:
	// x --> a --> b
	static float a=0;
	static float aAlt=0; 
	static float b=0;
	static float bAlt=0;
	const float tau = 3;

	if(reset)
	{
		a=0; aAlt=0;
		b=0; bAlt=0;
	}

	a = aAlt + (x-aAlt)/tau*dt;
	b = bAlt + (a-bAlt)/tau*dt;

	aAlt = a;
	bAlt = b;

	return b;
}

// Diese Funktion liefert eine soll-Winkelgeschwindigkeit in °/s zurück. 
// positiv = Linkskurve, negativ = Rechtskurve
float lenken()
{
	static bool links = false;
	static bool rechts = false;

	Sonar_measure(sonarL, 200);
	Sonar_measure(sonarR, 200);
	links = sonarL->distance_mm > 0;
	rechts = sonarR->distance_mm > 0;

	if(links)
		digitalWrite(LED_L_Pin, HIGH);
	else
		digitalWrite(LED_L_Pin, LOW);
	if(rechts)
		digitalWrite(LED_R_Pin, HIGH);
	else
		digitalWrite(LED_R_Pin, LOW);

	if(links && !rechts) // wenn links hinderniss, nach links ausweichen
	{
		return +75;
	}
	if(!links && rechts) // wenn rechts hinderniss, nach rechts ausweichen
	{
		return -75;
	}

	// sonst: richtung beibehalten
	return 0;
}

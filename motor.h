// motor.h


#ifndef __MOTOR_H
#define __MOTOR_H

#ifdef __cplusplus
extern "C" {
#endif


void Motor_L(int16_t geschwindigkeit); // +- 255, plus = nach vor, minus = zurück
void Motor_R(int16_t geschwindigkeit); // +- 255, plus = nach vor, minus = zurück
void Motor_beep(int32_t f, int32_t T_ms); // piepsen mit der Motorwicklung, frequenz f und dauer T

#ifdef __cplusplus
}
#endif

#endif


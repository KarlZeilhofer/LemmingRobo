// config.h

// Karl Zeilhofer, July 2015
// www.zeilhofer.co.at

// cody partly from:
// Kristian Lauszus, TKJ Electronics
// Web      :  http://www.tkjelectronics.com
// e-mail   :  kristianl@tkjelectronics.com

// This module enables easy printing and modification of various parameters (values)
// use it for development and debugging

// usage:
// define the parameter in this struct CFG_Parameters and 
// set it's defaults in CFG_restoreEEPROMValues()
// volatile parameters are not saved into EEPROM (name is starting with underscore)

// call CFG_userInput(char c) if the user has entered manipulation commands
// user inputs:
// 'L' list all parameters
// first send an activation char for the desired parameter
// then modify it's value with '+' and '-', '0'...'9' for printing column. 
// 'S' for saving all parameters to EEPROM
// 'V' for printing current value
// 'P' for global enabling/disabling printing columns




#include <stdint.h>

#ifndef __CONFIG_H
#define __CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#define CFG_MAX_NAME_LEN 12
#define CFG_MAX_UNIT_LEN 10

// 28*13
typedef struct parameter
{
  char name[CFG_MAX_NAME_LEN+1];
  char unit[CFG_MAX_UNIT_LEN+1];
  float v; // value
  float delta; // relativ oder absolut
  bool changeAbsolut_notRelative;
  char activationChar;
  uint8_t column; // 0 = no column, 1-9 = printing
}CFG_Parameter;

// This struct will store all the configuration values
typedef struct {
  CFG_Parameter dtMin; // minimale Abtastschrittweite in Sekunden

  CFG_Parameter phiNull; // getrimmter Nullwinkel (in °), bei dem der Robo seinen statischen Totpunkt hat. 
  CFG_Parameter phiSteuer; // vorgegebene Winkelabweichung, sodass der Robo in Bewegung versetzt werden kann. 
  CFG_Parameter phiLimit; // Ausschlagsgrenze des Steuerwinkels (in °)
  CFG_Parameter phiFehler; // Winkelfehler (in °)
  CFG_Parameter phiReg_P, phiReg_I, phiReg_D; // PID-Faktoren des Winkelreglers
  
  CFG_Parameter pwmMotor; // -1...+1
  CFG_Parameter pwmFilter_tau; // pwm wird Tiefpassgefiltert mit dieser Zeitkonstante (in s)
  CFG_Parameter kMotor; // Geschwindigkeit bei Vollgas: 193cm/3.03s = 0.637m/s
  CFG_Parameter vMotor; // ausgang des Tiefpassfilters, soll Motorgeschwindigkeit repräsentieren (normierte Recheneinheit, +1 = vollgas nach vorne)

  CFG_Parameter pos; // Integral über vMotor (in m)
  CFG_Parameter posSoll; // Vorgabeposition (in m)
  CFG_Parameter posFehler; // Abweichung (Reglereingang)
  CFG_Parameter posReg_P, posReg_I, posReg_D, posReg_DD; // PID-Faktoren des Positionsreglers

  CFG_Parameter motorEin; // sollte binär verwendet werden! 0 oder 1
  CFG_Parameter phiBeschl;
  CFG_Parameter omegaGyro;
  CFG_Parameter phiGyro;
  CFG_Parameter phiSelekt; // berechneter Winkel mit Selektivfilter

  // Reglerparameter für die Lenk-Regelung (Hochachsregler)
  CFG_Parameter omegaLenk_P;
  CFG_Parameter omegaLenk_I;

} CFG_Parameters;

extern CFG_Parameters P;

void CFG_init();
void CFG_printList();
void CFG_printTableHeader();
void CFG_printTableRow(float dt);
void CFG_updateEEPROM();
void CFG_userInput(char c);

extern float richtungsVersatz; // Beschreibung siehe .ino


#ifdef __cplusplus
}
#endif


#endif

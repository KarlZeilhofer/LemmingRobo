// imu.ino


#include "imu.h"
#include "i2c.h"
#include <Wire.h>
#include "tools.h"

#define IMU_ADDRESS 0x68 // I2C address of the MPU-6050

IMU_Objekt imu;



void IMU_einrichten()
{
	Wire.begin();
	Wire.setClock(400000UL); // Set I2C frequency to 400kHz


	while (i2cRead(0x75, i2cBuffer, 1));
	if (i2cBuffer[0] != 0x68) { // Read "WHO_AM_I" register
		Serial.print(F("Error reading sensor"));
		while (1); // Halt
	}

	while (i2cWrite(0x6B, 0x80, true)); // Reset device, this resets all internal registers to their default values
	do {
		while (i2cRead(0x6B, i2cBuffer, 1));
	} while (i2cBuffer[0] & 0x80); // Wait for the bit to clear
	delay(5);
	
	while (i2cWrite(0x6B, 0x09, true)); // PLL with X axis gyroscope reference, disable temperature sensor and disable sleep mode

	i2cBuffer[0] = 1; // Set the sample rate to 500Hz - 1kHz/(1+1) = 500Hz
	i2cBuffer[1] = 0x03; // Disable FSYNC and set 44 Hz Acc filtering, 42 Hz Gyro filtering, 1 KHz sampling
	i2cBuffer[2] = 0x00; // Set Gyro Full Scale Range to ±250deg/s
	i2cBuffer[3] = 0x00; // Set Accelerometer Full Scale Range to ±2g
	while (i2cWrite(0x19, i2cBuffer, 4, true)); // Write to all four registers at once

	delay(100); // Wait for the sensor to get ready
}

// Maßstab der Beschleunigungen: 32768 entsprechen 2g
// Maßstab der Drehgeschwindigkeiten: 32768 entsprechen 250°/s (???)
void IMU_werteEinlesen()
{
	Wire.beginTransmission(IMU_ADDRESS);
	Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
	Wire.endTransmission(false);
	Wire.requestFrom(IMU_ADDRESS,14,true);  // request a total of 14 registers
	imu.aX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
	imu.aY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	imu.aZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
	imu.Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
	imu.omegaX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
	imu.omegaY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
	imu.omegaZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
	Wire.endTransmission(true);

	imu.aX*=-1; // Vorzeichenumkehr für rechtsdrehendes Koordinatensystem
	imu.aY*=-1; // Vorzeichenumkehr für rechtsdrehendes Koordinatensystem
	imu.aZ*=-1; // Vorzeichenumkehr für rechtsdrehendes Koordinatensystem
}





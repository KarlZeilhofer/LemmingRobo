// pins.h


// Copyright 2015 Karl Zeilhofer
// www.zeilhofer.co.at

// This file is part of WolkenThermometer-firmware.

//     WolkenThermometer-firmware is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     WolkenThermometer-firmware is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with WolkenThermometer-firmware.  If not, see <http://www.gnu.org/licenses/>.


#include "pins.h"

// Stellt alle Pins auf Eingang oder Ausgang
void Pins_einrichten()
{
    // Pins als Ausgänge definieren:
    pinMode(Sonar_L_Trig_Pin, OUTPUT);  	digitalWrite(Sonar_L_Trig_Pin, LOW);   
    pinMode(Sonar_R_Trig_Pin, OUTPUT);		digitalWrite(Sonar_R_Trig_Pin, LOW);   

    // Motor-Pins sind Active-LOW
    pinMode(Motor_L_Plus_Pin, OUTPUT);		digitalWrite(Motor_L_Plus_Pin, HIGH);   
    pinMode(Motor_L_Minus_Pin, OUTPUT);	digitalWrite(Motor_L_Minus_Pin, HIGH);   
    pinMode(Motor_R_Plus_Pin, OUTPUT);		digitalWrite(Motor_R_Plus_Pin, HIGH);   
    pinMode(Motor_R_Minus_Pin, OUTPUT);			digitalWrite(Motor_R_Minus_Pin, HIGH);   
    
    pinMode(LED_Pin, OUTPUT);         digitalWrite(LED_Pin, LOW);   
    pinMode(LED_L_Pin, OUTPUT);         digitalWrite(LED_Pin, LOW);   
    pinMode(LED_R_Pin, OUTPUT);         digitalWrite(LED_Pin, LOW);   

    // Unbenutzte Pins werden auch auf Ausgang geschaltet. 
    pinMode(nc10_Pin, OUTPUT);          digitalWrite(nc10_Pin, LOW);   
    pinMode(nc11_Pin, OUTPUT);          digitalWrite(nc11_Pin, LOW);   
    pinMode(nc12_Pin, OUTPUT);          digitalWrite(nc12_Pin, LOW);   
    pinMode(ncA0_Pin, OUTPUT);          digitalWrite(ncA0_Pin, LOW);   
    pinMode(ncA1_Pin, OUTPUT);          digitalWrite(ncA1_Pin, LOW);   

    // Alle Eingänge einrichten:
    pinMode(Sonar_L_Echo_Pin, INPUT); 
    pinMode(Sonar_R_Echo_Pin, INPUT); 
 //   pinMode(IMU_Int_Pin, INPUT); 
}
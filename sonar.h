// sonar.h

// (c) Mechatronik Karl Zeilhofer
// www.zeilhofer.co.at

#ifndef __SONAR_H
#define __SONAR_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct sonar_object
{
	uint8_t triggerPin;
	uint8_t echoPin;
	int16_t duration_us;
	int16_t distance_mm; 
}Sonar_Object;


void Sonar_init(Sonar_Object* obj, uint8_t triggerPin, uint8_t echoPin);
void Sonar_measure(Sonar_Object* obj, int16_t maxLen_mm);


#ifdef __cplusplus
}
#endif

#endif
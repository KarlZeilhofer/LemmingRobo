// pins.h



// Copyright 2015 Karl Zeilhofer
// www.zeilhofer.co.at

// This file is part of WolkenThermometer-firmware.

//     WolkenThermometer-firmware is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     WolkenThermometer-firmware is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with WolkenThermometer-firmware.  If not, see <http://www.gnu.org/licenses/>.



#ifndef __PINS_H
#define __PINS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>


#define Rx_AVR_Pin 	0
#define Tx_AVR_Pin 	1
#define Sonar_L_Trig_Pin 	2 // Ausgang
#define Sonar_L_Echo_Pin 	7 // Eingang
#define Sonar_R_Trig_Pin 	4 // Ausgang
#define Sonar_R_Echo_Pin 	8 // Eingang
#define Motor_L_Minus_Pin	6 // Ausgang
#define Motor_L_Plus_Pin	3 // Ausgang
#define Motor_R_Minus_Pin	5 // Ausgang
#define Motor_R_Plus_Pin	9 // Ausgang
//#define IMU_Int_Pin	10 // Eingang, Interrupt
#define nc10_Pin	10 // Ausgang, unbenutzt
#define nc11_Pin	11 // Ausgang, unbenutzt
#define nc12_Pin	12 // Ausgang, unbenutzt
#define LED_Pin		13 // Ausgang, eingeschaltet wenn high


#define ncA0_Pin	A0 // Ausgang, unbenutzt
#define ncA1_Pin	A1 // Ausgang, unbenutzt
#define LED_R_Pin	A2
#define LED_L_Pin	A3
// A4 = SDA (zur IMU)
// A5 = SCL (zur IMU)



void Pins_einrichten();

#ifdef __cplusplus
}
#endif


#endif